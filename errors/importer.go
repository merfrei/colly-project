package errors

// ImporterError represents an error in the Importer
type ImporterError struct {
	err error
}

func (ie *ImporterError) Error() string {
	return ie.err.Error()
}

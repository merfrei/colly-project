package errors

import (
	"fmt"
	"io"
	"sync"

	"github.com/pkg/errors"
)

// Manager will manage all of the errors
type Manager struct {
	errors chan error
	writer io.Writer
}

// Get the error by type
func (m *Manager) Get(err error, errT string) error {
	switch errT {
	case "importer":
		return &ImporterError{err: err}
	default:
		return err
	}
}

// Add append a new error by type
func (m *Manager) Add(err error, errT string, msg string) {
	m.errors <- errors.Wrap(m.Get(err, errT), msg)
}

// Start should be used to start processing the errors
func (m *Manager) Start(wg *sync.WaitGroup) {
	defer wg.Done()
	for err := range m.errors {
		var errorL string
		switch err := errors.Cause(err).(type) {
		case *ImporterError:
			errorL = "ImporterError: " + err.Error()
		default:
			errorL = "Unknown error: " + fmt.Sprintf("%+v", err)
		}
		m.writer.Write([]byte(errorL + "\n"))
	}
}

// Close errors
func (m *Manager) Close() {
	close(m.errors)
}

// NewManager create and return a new errors Manager
func NewManager(w io.Writer, errors chan error) *Manager {
	return &Manager{
		errors: errors,
		writer: w,
	}
}

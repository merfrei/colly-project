package importer

import (
	"context"
	"errors"
	"sync"

	cerrors "gitlab.com/merfrei/colly-project/errors"
	"gitlab.com/merfrei/colly-project/items"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoImporter is an implementation of the Importer for a MongoDB database
type MongoImporter struct {
	collection *mongo.Collection
	opts       *options.FindOneAndUpdateOptions
}

// NewMongoImporter create and return a new MongoImporter instance
func NewMongoImporter(collection *mongo.Collection, opts *options.FindOneAndUpdateOptions) *MongoImporter {
	if opts == nil {
		opts = options.FindOneAndUpdate().SetUpsert(true) // UPSERT is the default
	}
	return &MongoImporter{
		collection: collection,
		opts:       opts,
	}
}

// ImportItems will import the items in a MongoDB database
func (imp *MongoImporter) ImportItems(wg *sync.WaitGroup, itemsChan <-chan interface{}, errorsM *cerrors.Manager) <-chan interface{} {
	importedItems := make(chan interface{})

	go func() {
		defer wg.Done()
		defer close(importedItems)
		defer errorsM.Close()

		for item := range itemsChan {
			itemI, ok := item.(items.Item)
			if ok == false {
				errorsM.Add(errors.New("Item is not a valid item interface"), "importer", "Importer error")
				importedItems <- item
				continue
			}
			filter := bson.D{{Key: "identifier", Value: itemI.GetIdentifier()}}
			update := bson.D{{Key: "$set", Value: item}}
			newItem := itemI.GetT()
			err := imp.collection.FindOneAndUpdate(context.Background(), filter, update, imp.opts).Decode(newItem)
			if err != nil {
				if err == mongo.ErrNoDocuments {
					itemI.SetStatus("new")
				} else {
					errorsM.Add(err, "", "Importer error") // An unexpected error (bug)
				}
			} else {
				itemI.SetStatus("updated")
			}
			importedItems <- item
		}
	}()

	return importedItems
}

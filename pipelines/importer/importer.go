// Package importer contains different types of importer for the crawlers
package importer

import (
	"fmt"
	"os"
	"sync"

	"gitlab.com/merfrei/colly-project/errors"
	cerrors "gitlab.com/merfrei/colly-project/errors"
)

// Importer is the base interface to implement for all the importers
type Importer interface {
	ImportItems(*sync.WaitGroup, <-chan interface{}, *cerrors.Manager) <-chan interface{}
}

func printItems(wg *sync.WaitGroup, items <-chan interface{}) {
	defer wg.Done()
	ix := 0
	for item := range items {
		ix++
		fmt.Println(ix, " - Scraped: ", item)
	}
}

// BasicPipeline take an importer, import the items and display them to stdout
func BasicPipeline(imp Importer, items <-chan interface{}) *sync.WaitGroup {
	var wg sync.WaitGroup
	// Importer
	impErrsC := make(chan error)
	impErrsM := errors.NewManager(os.Stdout, impErrsC)
	wg.Add(2)
	go printItems(&wg, imp.ImportItems(&wg, items, impErrsM))

	// Errors
	wg.Add(1)
	go impErrsM.Start(&wg)

	return &wg
}

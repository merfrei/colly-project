package main

import (
	"flag"
	"io/ioutil"
	"log"
	"regexp"
	"strings"

	"github.com/gocolly/colly"
	"gitlab.com/merfrei/colly-project/items"
	"gitlab.com/merfrei/colly-project/pipelines/importer"
	"gitlab.com/merfrei/colly-project/pkg/config"
	mongoM "gitlab.com/merfrei/colly-project/pkg/mongo"
)

const limitPages = 1

func main() {
	// ---- INIT ----
	configFile := flag.String("config", "config.toml", "Path to the config file")
	collectionName := flag.String("collection", "politifact", "The collection when the items will be stored")

	flag.Parse()

	configD, err := ioutil.ReadFile(*configFile)
	if err != nil {
		log.Fatal(err)
	}
	config := config.Load(configD)

	mongoCli, err := mongoM.Connect(config.Mongo.URI)
	if err != nil {
		log.Fatal(err)
	}
	defer mongoM.Disconnect(mongoCli)

	db := mongoCli.Database(config.Mongo.Database)
	col := db.Collection(*collectionName)

	itemsChan := make(chan interface{})

	imp := importer.NewMongoImporter(col, nil)
	pipelines := importer.BasicPipeline(imp, itemsChan)

	// ---- ----

	// ---- CRAWLER ----

	c := colly.NewCollector(
		colly.Async(true),
		colly.AllowedDomains("politifact.com", "www.politifact.com"),
	)

	c.Limit(&colly.LimitRule{DomainGlob: "*", Parallelism: 4})

	articleCollector := c.Clone()

	c.OnRequest(func(r *colly.Request) {
		log.Println("visiting", r.URL.String())
	})

	c.OnHTML(".m-statement__quote a[href]", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		articleURL := e.Request.AbsoluteURL(link)
		log.Println("Link found:", articleURL)
		articleCollector.Visit(articleURL)
	})

	sdre := regexp.MustCompile(`\s(\w+\s\d+,\s\d+)\s`)

	articleCollector.OnHTML("main", func(h *colly.HTMLElement) {
		url := h.Request.URL.String()
		log.Println("Article found", url)
		splits := strings.Split(url, "/")
		var identifier string
		for i := len(splits) - 1; i >= 0; i-- {
			if splits[i] != "" {
				identifier = splits[i]
				break
			}
		}
		article := items.Article{
			Identifier:    identifier,
			URL:           url,
			Title:         h.ChildText("h2.c-title"),
			Author:        h.ChildText(".m-author__content > a"),
			PublishedDate: h.ChildText(".m-author__date"),
			Claim:         strings.TrimSpace(h.DOM.Find(".m-statement__quote-wrap > .m-statement__quote").First().Text()),
			Rating:        h.ChildAttr(".m-statement__meter .c-image__original", "alt"),
			Tags:          []string{},
			Sources:       []string{},
		}
		dateStr := h.ChildText(".m-statement__desc")
		matches := sdre.FindStringSubmatch(dateStr)
		if len(matches) > 1 {
			article.ClaimDate = matches[1]
		}
		h.ForEach("ul.m-list.m-list--horizontal a.c-tag", func(_ int, a *colly.HTMLElement) {
			article.Tags = append(article.Tags, a.ChildText("span"))
		})
		h.ForEach("#sources article p", func(_ int, s *colly.HTMLElement) {
			article.Sources = append(article.Sources, s.Text)
		})

		itemsChan <- &article
	})

	c.Visit("https://www.politifact.com/factchecks/")

	// ---- ----

	c.Wait()
	articleCollector.Wait()

	close(itemsChan)

	pipelines.Wait()
}

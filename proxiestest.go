package main

import (
	"context"
	"fmt"
	"sync"
	"time"
)

// Proxy representation
type Proxy struct {
	ID  int
	URL string
}

func main() {
	proxiesD := make([]Proxy, 0)

	var blockedD sync.Map

	baseP := 8110
	for i := 0; i < 10; i++ {
		proxiesD = append(proxiesD, Proxy{
			ID:  i + 1,
			URL: fmt.Sprintf("http://127.0.0.1:%d", i+baseP),
		})
	}

	poolLenth := len(proxiesD)

	proxiesC := make(chan *Proxy, poolLenth)
	blockedC := make(chan *Proxy)

	ctx, closeCrawler := context.WithCancel(context.Background())

	go func() {
		// Process blocked proxies
		for p := range blockedC {
			blockedD.Store(p.ID, true)
		}
	}()

	blockedLimit := len(proxiesD)

	go func() {
		// Sender
		defer close(proxiesC)
		current := 0
		bll := blockedLimit
		for {
			select {
			case <-ctx.Done():
				return
			default:
				if current >= len(proxiesD) {
					current = 0
				}
				proxy := &proxiesD[current]
				if _, ok := blockedD.Load(proxy.ID); ok && bll > 0 {
					fmt.Println("Ignoring Blocked: ", proxy.ID)
					bll--
					current++
					continue
				}
				if bll == 0 {
					fmt.Println("Max blocked limit reached")
					blockedD.Delete(proxy.ID)
				}
				bll = blockedLimit
				proxiesC <- proxy
				current++
			}
		}
	}()

	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()
		name := "Collector 1"
		for i := 0; i < 13; i++ {
			p := <-proxiesC
			fmt.Println(name, ": ", i+1, " > ", p)
			time.Sleep(1 * time.Second)
			if i == 2 {
				blockedC <- p
			}
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		name := "Collector 2"
		for i := 0; i < 8; i++ {
			p := <-proxiesC
			fmt.Println(name, ": ", i+1, " > ", p)
			time.Sleep(1 * time.Second)
			if i == 1 {
				blockedC <- p
			}
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		name := "Collector 3"
		for i := 0; i < 8; i++ {
			p := <-proxiesC
			fmt.Println(name, ": ", i+1, " > ", p)
			time.Sleep(1 * time.Second)
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		name := "Collector 4"
		for i := 0; i < 12; i++ {
			p := <-proxiesC
			fmt.Println(name, ": ", i+1, " > ", p)
			time.Sleep(1 * time.Second)
			if i%2 == 0 {
				blockedC <- p
			}
		}
	}()

	wg.Wait()

	closeCrawler()
}

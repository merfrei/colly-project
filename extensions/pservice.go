package extensions

import (
	"github.com/gocolly/colly"
	"gitlab.com/merfrei/colly-project/pkg/pservice"
)

// ProxyServiceExtension support proxy management via Proxy Service API
func ProxyServiceExtension(c *colly.Collector, ps *pservice.PService) {
	c.OnRequest(func(r *colly.Request) {

	})
}

// Package pservice is a Proxy Service API utility
package pservice

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"path"
	"strconv"
	"sync"
	"time"

	"github.com/gocolly/colly"
)

// Default pool size if a length paramater is not specified
const poolSize = 1000

// Proxy returned by the service
type Proxy struct {
	ID  string `json:"id"`
	URL string `json:"url"`
}

// PService allow you to interact with the Proxy Service API
// to use a pool of proxies
//
// ps := &pservice.New(apiURL, apiKey, target)
// extensions.ProxyServiceExtension(c, ps)
// err := ps.Start()
//
// ....
// If you want to add proxies as blocked...
// ps.Blocked <-proxy (Not possible at the moment) TODO
//
// ps.Stop()
//
type PService struct {
	url      string
	apiKey   string
	target   string
	pool     []Proxy
	excluded sync.Map
	Proxies  chan *Proxy
	Blocked  chan *Proxy
	client   *http.Client
}

// New create and return a new PService instance
func New(url string, apiKey string, target string) *PService {
	ps := &PService{
		url:    url,
		apiKey: apiKey,
		target: target}
	timeout := time.Duration(30 * time.Second)
	ps.client = &http.Client{
		Timeout: timeout,
	}
	return ps
}

// FetchProxies fetch a new list of proxies for the target
func (ps *PService) FetchProxies(query map[string]string) error {
	url, err := url.Parse(ps.url)
	url.Path = path.Join(url.Path, ps.target)
	req, err := http.NewRequest("GET", url.String(), nil)
	if err != nil {
		return err
	}

	q := req.URL.Query()
	for param, value := range query {
		q.Set(param, value)
	}
	l := q.Get("len")
	if l == "" {
		q.Set("len", strconv.Itoa(poolSize))
	}
	req.URL.RawQuery = q.Encode()

	resp, err := ps.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode != 200 {
		return fmt.Errorf("No 200 response found: %s", body)
	}

	err = json.Unmarshal(body, &ps.pool)
	if err != nil {
		return err
	}

	return nil
}

// ProcessBlocked check for new blocked proxies
func (ps *PService) ProcessBlocked(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		case p := <-ps.Blocked:
			ps.excluded.Store(p.ID, true)
		}
	}
}

// Cycle send proxies to the pool until a Stop signal is received
func (ps *PService) Cycle(ctx context.Context) {
	poolLenth := len(ps.pool)
	ps.Proxies = make(chan *Proxy, poolLenth)
	defer close(ps.Proxies)
	current := 0
	bll := poolLenth
	for {
		select {
		case <-ctx.Done():
			return
		default:
		}
		if len(ps.pool) == 0 {
			log.Println("Proxy Pool is empty")
			ps.Proxies <- nil
			continue
		}
		if current >= len(ps.pool) {
			current = 0
		}
		proxy := &ps.pool[current]
		if _, ok := ps.excluded.Load(proxy.ID); ok && bll > 0 {
			log.Println("Ignoring Blocked: ", proxy.ID)
			bll--
			current++
			continue
		}
		if bll == 0 {
			log.Println("Max blocked limit reached")
			ps.excluded.Delete(proxy.ID)
		}
		bll = poolLenth
		ps.Proxies <- proxy
		current++
	}
}

// GetProxy is the function to set in SetProxyFunc collector
func (ps *PService) GetProxy(pr *http.Request) (*url.URL, error) {
	p := <-ps.Proxies
	parsedU, err := url.Parse(p.URL)
	if err != nil {
		return nil, err
	}
	ctx := context.WithValue(pr.Context(), colly.ProxyURLKey, p.URL)
	*pr = *pr.WithContext(ctx)
	return parsedU, nil
}

// Start initialize the goroutines and make the proxies availables
// for the collector
func (ps *PService) Start(ctx context.Context, query map[string]string) error {
	err := ps.FetchProxies(query)
	if err != nil {
		return err
	}
	go ps.ProcessBlocked(ctx)
	go ps.Cycle(ctx)

	return err
}
